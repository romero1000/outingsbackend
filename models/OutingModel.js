'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
var AddressSchema = require('./AddressModel')
var UserSchema = require('./UserModel')
var User = mongoose.model('User')
var Address = mongoose.model('Address')

const OutingSchema = new mongoose.Schema({
	id_user_creator: { type: Schema.ObjectId, ref: 'User' },
	category: { type: Object, required: true },
	description: { type: String, required: true },
	address:{
		type: Schema.ObjectId,
		ref: 'Address'
	}
})

module.exports = mongoose.model('Outing', OutingSchema)