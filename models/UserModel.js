'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bcrypt = require('bcrypt')
const saltRounds = 15

const UserSchema = new mongoose.Schema({
	name: {type: String, required: true},
	username: {type: String, required: true, unique:true},
	email: {type: String, required: true, unique:true},
	password: {type: String, required: true},
	age: {type: Number, required: true},
	gender: {type: String, required: true},
	region: {type: String, required: true},
	country: {type: String, required: true},
	lenguague: {type: String, required: true},
	avatar: {type: String, required:false}
})

UserSchema.pre('save',function(next){
	if(this.isNew || this.isModified('password')){
		const document = this
		bcrypt.hash(document.password, saltRounds, (err, hashedPassword)=>{
			if(err){
				next(err)
			}else{
				document.password = hashedPassword
				next()
			}
		})
	}else{
		next()
	}
})

UserSchema.methods.isCorrectPassword = function(candidatePassword, callback){
	bcrypt.compare(candidatePassword, this.password, function(err, same){
		if(err){
			callback(err)
		}else{
			callback(err, same)
		}
	})
}

module.exports = mongoose.model('User', UserSchema)