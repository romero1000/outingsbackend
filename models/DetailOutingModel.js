'use strict'

const mongoose = require('mongoose')
const OutingSchema = require('./OutingModel')
const UserSchema = require('./UserModel')
const Outing = mongoose.model('Outing')
const User = mongoose.model('User')

const DetailOutingSchema = new mongoose.Schema({
	outing_id: {type: mongoose.Schema.ObjectId, ref: 'Outing'},
	users_signed: [{ type: mongoose.Schema.ObjectId, ref: 'User'}],
	space: { type: Number, required: true },
	hour: { type: String, required: true },
	duration: {type: String, required: true}
})

module.exports = mongoose.model('DetailOuting', DetailOutingSchema)