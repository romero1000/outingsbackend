'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AddressSchema = new Schema({
	addres: { type:String, required: true },
	coordinates: { type:String, required: true },
	location: { type:String, required: true },
	references: { type:String, required: true },
	images_ref: { type:String, required: false }
})

module.exports = mongoose.model('Address', AddressSchema)