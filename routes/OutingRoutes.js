'use strict'
var express = require('express')
var OutingController = require('../controllers/OutingController')
var router = express.Router()

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({ uploadDir: './uploads/address' });

router.get('/home', OutingController.home)
router.post('/save-outing', OutingController.saveOuting)
router.post('/upload-address-ref/:id', multipartMiddleware, OutingController.uploadAddressImages)

module.exports = router