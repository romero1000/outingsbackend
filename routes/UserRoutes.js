'use strict'
var express = require('express')
var UserController = require('../controllers/UserController')
var router = express.Router()

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({ uploadDir: './uploads/users' });

router.get('/home', UserController.home)
router.post('/sign_up', UserController.signUpUser)
router.post('/authenticate', UserController.signInUser)
router.get('/:id?', UserController.getUser)
router.post('/upload-avatar/:id', multipartMiddleware, UserController.uploadImage)
router.get('/get-image/:image', UserController.getAvatarFile)


module.exports = router