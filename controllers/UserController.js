'use strict'

var User = require('../models/UserModel')
var jwt = require('jsonwebtoken')
var config = require('../config')

var fs = require('fs')
var path = require('path')

var controller = {
	home: function(req, res){
		return res.status(200).send({
			message: 'Bienvenido a outings milton'
		})
	},

	signUpUser: function(req, res){
		const {name, username, email, password, age, gender, region, country, lenguague, avatar} = req.body
		const user = new User({name, username, email, password, age, gender, region, country, lenguague, avatar})
		
		user.save((err, userStored) => {
			if(err) return res.status(500).send({message: 'Error al guardar el usuario'})

			if(!userStored) return res.status(404).send({message: 'No se ha podido guardar el usuario'})

			const token = jwt.sign({id:userStored._id}, config.SECRET, {
				expiresIn: 86400 // 24 horas esta en segundos
			})

			return res.status(200).send({token: token})
		})
	},

	signInUser: function(req, res){
		const {email, password} = req.body
		console.log("ESTE ES EL EMAIL")
		console.log(email)
		console.log(password)
		User.findOne({email: email}, (err, user)=>{
			if(err){
				return res.status(500).send({message: 'Error al buscar usuario'})
			}else if(!user){
				return res.status(404).send({message:'Correo no registrado'})
			}else{
				user.isCorrectPassword(password, (err, result)=>{
					if(err){
						return res.status(500).send({message:'Error al verificar contraseña'})
					}else if(result){
						const token = jwt.sign({id:user._id}, config.SECRET, {
							expiresIn: 86400 // 24 horas esta en segundos
						})
						return res.status(200).send({token:token})
					}else{
						return res.status(404).send({message:'Contraseña Incorrecta'})
					}
				})
			}
		})
	},

	getUser: function(req, res){
		var userId = req.params.id;

		if(userId == null) return res.status(404).send({message: 'El usuario no existe.'});

		User.findById(userId, (err, user) => {

			if(err) return res.status(500).send({message: 'Error al devolver los datos.'});

			if(!user) return res.status(404).send({message: 'El usuario no existe.'});

			return res.status(200).send({
				user
			});

		});
	},

	uploadImage: function(req, res){
		let userId = req.params.id;

		console.log("========archivos========")
		console.log(req.files)
		if(req.files){
			let filePath = req.files.avatar.path;
			let fileSplit = filePath.split('\\');
			let fileName = fileSplit[2];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){

				User.findByIdAndUpdate(userId, {avatar: fileName}, {new: true}, (err, userUpdated) => {
					if(err) return res.status(500).send({message: 'El avatar no se ha subido'});

					if(!userUpdated) return res.status(404).send({message: 'El usuario no existe y no se ha asignado la imagen'});

					return res.status(200).send({
						user: userUpdated
					});
				});

			}else{
				fs.unlink(filePath, (err) => {
					return res.status(200).send({message: 'La extensión no es válida'});
				});
			}

		}else{
			return res.status(200).send({
				message: 'Avatar no subido'
			});
		}
	},

	getAvatarFile: function(req, res){
		let file = req.params.image;
		let path_file = './uploads/users/'+file;

		fs.stat(path_file, function(err, stats){
			if(err){
				console.log("ERROR OCURRIDO")
				console.log(err)
				return res.status(200).send({
					message: "No existe la imagen..."
				})
			}else{
				return res.sendFile(path.resolve(path_file))
			}
		})
	}
}

module.exports = controller