'use strict'

var Outing = require('../models/OutingModel')
var DetailOuting = require('../models/DetailOutingModel')
var Address = require('../models/AddressModel')

var fs = require('fs')
var path = require('path')

var controller = {
	home: function(req, res){
		return res.status(200).send({
			message: 'Bienvenido a outings milton'
		})
	},

	saveOuting: function(req, res){
		const { id_user_creator, category, description, address, detail_out} = req.body
		const outing = new Outing({ id_user_creator, category, description})
		const _address = new Address(address)
		outing.address = _address._id
		const detail_outing = new DetailOuting(detail_out)

		_address.save((err, addressStored) => {
			//console.log(err)
			if(err) return res.status(500).send({message: 'Error al guardar la direccion'})

			if(!addressStored) return res.status(404).send({message: 'No se ha podido guardar la direccion'})

			outing.save((err, outingStored) => {
				//console.log(err)
				if(err) return res.status(500).send({message: 'Error al guardar el Outing'})

				if(!outingStored) return res.status(404).send({message: 'No se ha podido guardar el Outing'})

				detail_outing.save((err, dtOutStored)=>{
					if(err) return res.status(500).send({message: 'Error al guardar el detalle de la salida'})
					if(!dtOutStored) return res.status(404).send({message: 'No se ha podido guardar el detalle de salida'})

					return res.status(200).send({outing: outingStored, address: addressStored, detail_outing: dtOutStored})
				})
			})
		})
	},

	uploadAddressImages: function(req, res){
		let addressId = req.params.id;

		if(req.files){
			let filePath = req.files.images_ref.path;
			let fileSplit = filePath.split('\\');
			let fileName = fileSplit[2];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){

				Address.findByIdAndUpdate(addressId, {images_ref: fileName}, {new: true}, (err, addressUpdated) => {
					if(err) return res.status(500).send({message: 'La Imagen no se ha subido'});

					if(!addressUpdated) return res.status(404).send({message: 'La direccion no existe y no se ha asignado la imagen'});

					return res.status(200).send({
						address: addressUpdated
					});
				});

			}else{
				fs.unlink(filePath, (err) => {
					return res.status(200).send({message: 'La extensión no es válida'});
				});
			}

		}else{
			return res.status(200).send({
				message: 'Imagen no subido'
			});
		}
	}

}

module.exports = controller